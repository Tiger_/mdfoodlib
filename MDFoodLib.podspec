Pod::Spec.new do |s|

  s.name         = "MDFoodLib"
  s.version      = "0.0.3"
  s.summary      = "A short description of MDFoodLib."

  s.description  = <<-DESC
                   A longer description of MDFoodLib in Markdown format.
                   DESC

  s.homepage     = "http://EXAMPLE/MDFoodLib"

  s.license      = "MIT (example)"
  # s.license      = { :type => "MIT", :file => "FILE_LICENSE" }
  
  s.author             = { "程虎" => "541887133@qq.com" }
  # Or just: s.author    = "程虎"
  # s.authors            = { "程虎" => "541887133@qq.com" }
  # s.social_media_url   = "http://twitter.com/程虎"


  # s.platform     = :ios
  s.platform     = :ios, "7.0"

  # s.ios.deployment_target = "5.0"
  # s.osx.deployment_target = "10.7"

  s.source       = { :git => "https://Cheng_Hu@bitbucket.org/Cheng_Hu/mdfoodlib.git", :tag => '0.0.2' }

  s.source_files  = "MDFoodLib/**/*.{h,m}"
  #  s.exclude_files = "Classes/Exclude"

  # s.public_header_files = "Classes/**/*.h"

  # s.resource  = "icon.png"
  # s.resources = "MDFoodLib/Resources/*.sqlite"

  s.ios.resource_bundle = { 'FoodDB' => 'MDFoodLib/**/*.sqlite' }
  # s.preserve_paths = "FilesToSave", "MoreFilesToSave"

  # s.framework  = "SomeFramework"
  s.frameworks = "SystemConfiguration"

  s.library   = "libsqlite3.0"
  # s.libraries = "iconv", "xml2"

  s.requires_arc = true

  # s.xcconfig = { "HEADER_SEARCH_PATHS" => "$(SDKROOT)/usr/include/libxml2" }
  # s.dependency "JSONKit", "~> 1.4"

end
