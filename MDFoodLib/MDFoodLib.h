//
//  MDFoodLib.h
//  MDFoodLib
//
//  Created by 程虎 on 14-7-28.
//  Copyright (c) 2014年 user. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MDFoodLib : NSObject

+(NSString *)sayHello;
+(NSArray *)getFoodsByName:(NSString *)name;

@end
