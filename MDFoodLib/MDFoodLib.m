//
//  MDFoodLib.m
//  MDFoodLib
//
//  Created by 程虎 on 14-7-28.
//  Copyright (c) 2014年 user. All rights reserved.
//

#import "MDFoodLib.h"
#import <sqlite3.h>


@implementation MDFoodLib

+(NSString *)sayHello {
    return @"Hello";
}

+(NSArray *)getFoodsByName:(NSString *)name {
    return [self queryFoodMyName:@"egg"];
}

+ (NSArray *)queryFoodMyName:(NSString *)name {
    sqlite3 *database;
    
    NSString *path = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"FoodDB.bundle/default.sqlite"];//[[NSBundle mainBundle] pathForResource:@"default" ofType:@"sqlite"];
    
    if (sqlite3_open([path UTF8String], &database) != SQLITE_OK) {
        
        NSLog(@"[SQLITE] Unable to open database!");
        return nil; // if it fails, return nil obj
    }
    
    NSString *queryString = @"select * from food;";
    
    NSArray *array = [self performQuery:queryString fromDB:database];
    
    return array;
}

+(NSArray *)performQuery:(NSString *)query fromDB:(sqlite3*)database{
    sqlite3_stmt *statement = nil;
    const char *sql = [query UTF8String];
    if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"[SQLITE] Error when preparing query!");
    } else {
        NSMutableArray *result = [NSMutableArray array];
        while (sqlite3_step(statement) == SQLITE_ROW) {
            NSMutableArray *row = [NSMutableArray array];
            for (int i=0; i<sqlite3_column_count(statement); i++) {
                int colType = sqlite3_column_type(statement, i);
                id value;
                if (colType == SQLITE_TEXT) {
                    const unsigned char *col = sqlite3_column_text(statement, i);
                    value = [NSString stringWithFormat:@"%s", col];
                } else if (colType == SQLITE_INTEGER) {
                    int col = sqlite3_column_int(statement, i);
                    value = [NSNumber numberWithInt:col];
                } else if (colType == SQLITE_FLOAT) {
                    double col = sqlite3_column_double(statement, i);
                    value = [NSNumber numberWithDouble:col];
                } else if (colType == SQLITE_NULL) {
                    value = [NSNull null];
                } else {
                    NSLog(@"[SQLITE] UNKNOWN DATATYPE");
                }
                
                [row addObject:value];
            }
            [result addObject:row];
        }
        return result;
    }
    return nil;
}

@end
